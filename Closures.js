
//counter Factory
function counterFactory() {
    let counter = 5
    function Inc()
    {
        return counter=counter+1;
    }
    function Dec()
    {
        return counter=counter-1;
    }

    return {
      increment: function ()
       { 
          return Inc()
        },
      decrement: function ()
       { 
            return Dec()
        }
  
    };
}

//Limit Function Call

  function limitFunctionCallCount(cb, n)
   { 
    return (

       function () 
      {
        for(let i=0;i<n;i++)
        {
            console.log(cb(i))
          
        }
        
        } )
    }
      


  module.exports = {
    counterFactory: counterFactory,limitFunctionCallCount: limitFunctionCallCount
  }
  