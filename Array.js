//each 
function each(elements, cb) {
    let empArray = []
    if ((cb) && (Array.isArray(elements)) && (elements.length))
    {
        for (let i = 0; i < elements.length; i++) 
        {
            cb(elements[i], i);
        }
    }
    else if(!elements && !cb)
     console.log('Array and callback not exist')
    else if (!Array.isArray(elements) )
    {
        console.log('Array does not exist')
    }
    else if (!cb ) {
        console.log('callback function does not exist')
    }
    else 
    {
        console.log(empArray);
    }
}


//map
function map(elements, cb) {
    let mapArray = []
    let empArray = []
    if ((cb) && (Array.isArray(elements)) && (elements.length)) {
        for (let i = 0; i < elements.length; i++)
        {
            mapArray.push(cb(elements[i]));
        }
    
        return mapArray
    }
    else if(!elements && !cb)
     return
    else if (!(cb)) {
        return ("Call back function does not exist")
    }
    else if (!Array.isArray(elements)) {
        return ("array does not exist")
    }
    else if (!(Array.isArray(elements).length == 0) && (cb)) {
        return (empArray)
    }
    else {
        return mapArray
    }
}


//reduce
function reduce(elements, cb, startingValue) {
    let i;
    let empArray=[]
    if ((cb) && (Array.isArray(elements)) && (elements.length)) 
    {
        startingValue=elements[0];
        for (i=1; i < elements.length; i++)
         {
            red= cb(startingValue, elements[i])
              startingValue = red;
        }
        return red;
    }
    else if(!elements && !cb)
    return
   else if (!(cb)) {
       return ("Call back function does not exist")
   }
   else if (!Array.isArray(elements)) {
       return ("array does not exist")
   }
   else if (!(Array.isArray(elements).length == 0) && (cb)) {
       return (empArray)
   }
    
}


//find
function find(elements, cb) 
{   let i;
    let empArray=[]
    if ((cb) && (Array.isArray(elements)) && (elements.length)) {
        let found=0;
        for (i=0; i < elements.length; i++) 
        {
            if (cb(elements[i])) 
            {
                found= 1
                break;
            }
        }
        if(found==1)
        return elements[i];
        else
        return 
    }
    else if(!elements && !cb)
    return
   else if (!(cb)) {
       return ("Call back function does not exist")
   }
   else if (!Array.isArray(elements)) {
       return ("array does not exist")
   }
   else if (!(Array.isArray(elements).length == 0) && (cb)) {
       return (empArray)
   }
   
}


//filter
function filter(elements, cb)
 {
    let fil = []
    let empArray=[]
    if((cb) && (Array.isArray(elements)) && (elements.length)) {
        for (let i = 0; i < elements.length; i++) {
            if(cb(elements[i]))
              fil.push(elements[i]);
        }
        return fil
    }
    else if(!elements && !cb)
     return
    else if ((elements.length == 0) && (cb))
         return (empArray)
    else if (!(cb))
        return ('callback function does not exist')
    else if (!Array.isArray(elements))
        return ("array does not exist")
    } 


//flatten
function flatten(elements,res=[]) 
{   
    let empArray=[];
    if(!elements)
    {
       return
    }
    else if( (elements) && elements.length==0) 
      return empArray;
    else
    {
        for(let i=0;i<elements.length;i++)
        {
        if(!Array.isArray(elements[i]))
        res.push(elements[i])
        else
        flatten(elements[i],res)
        }
      return res
    }
}

//module exports
module.exports = {each: each, map: map,  reduce: reduce, find: find, filter: filter,flatten: flatten
}