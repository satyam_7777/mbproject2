let closure=require('./Closures.js')


//counterFactory Function
console.log('\ncounter Factory')
console.log(closure.counterFactory().increment())
console.log(closure.counterFactory().decrement())


//limitFunctionCallCount
console.log('\nlimit Function CallCount')

//cb function
function cb(i)
{
    return i*i;
}
//res is a  returned function
const res= closure.limitFunctionCallCount(cb,10)
res();
