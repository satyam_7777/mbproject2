//objects

//keys
function keys(obj) 
{
    let objKey = [];
    if(!(obj))
    {
        return
    }
   else if (obj) 
   {
        for (const property in obj) 
        {
            objKey.push(property)
        }
    
      return objKey
    }
    
}

//values
function values(obj) {
    let objValue = [];
    if(!(obj))
    {
        return
    }
   else if (obj) {
        for (const property in obj) 
        {
            objValue.push(obj[property])
        }
       return objValue
    }
    
}



//map object
function mapObject(obj, cb) 
{
    if ((obj) && (cb)) {
        let objMap = {}
        for (const property in obj)
        {
            objMap[property] = cb(obj[property])
        }
        return objMap
    }
    else if(!cb && (obj))
        return 'callback function does not exist'
    else
        return
}



//pairs
function pairs(obj) {
    if(!obj)
    {
        return
    }
    else if (obj) {
        let objPairs = []

        for (const property in obj) {
            const temp = []
            temp.push(property,obj[property])
            objPairs.push(temp)
        }
        return objPairs
    }
}

/* STRETCH PROBLEMS */

//Object Invert
function invert(obj) 
{
    let objInvert = {};
    if (!obj)
    {
        return
    }
    else if ((obj))
    {
        for (let property in obj) 
        {
            objInvert[obj[property]] = property;
        }
    return (objInvert)
    }
}



// object Defaults
function defaults(obj,defaultProps)
 {
    if(!obj && !defaultProps)
    {
       return
    }
    else if(obj && defaultProps)
    {
          let found;
      for (const propKey in defaultProps) 
      {
            found = 0
            for (const objKey in obj)
             {
                if (propKey === objKey) 
                {
                    found = 1
                    break;
                }
            }
            if (found == 0)
            {
               obj[propKey] = defaultProps[propKey]
            }
        }
        return obj

    }

}

module.exports = {keys: keys,values: values,mapObject: mapObject,pairs: pairs,invert: invert,
    defaults: defaults
}
