const objData = require('./objectData.js')
const object= require('./Object.js')



//keys
console.log('\nObject Keys')

let resKey=object.keys(objData)
console.log(resKey)


//values
console.log('\nObject Values')

let resValue = object.values(objData)
console.log(resValue)



//map Object
console.log('\nMap Object')
// map aguments   
//  mapObject(objData,cb)

function cb(value)
{
  return (value+' & '+value)
}
const resMap=object.mapObject(objData,cb);
console.log(resMap);



//object pairs
console.log("\nObject Pairs")

const objPairs = object.pairs(objData);
console.log(objPairs)


/* STRETCH PROBLEMS */
//object invert
console.log('\nObject Invert')

const objInvert =object.invert(objData)
console.log(objInvert)



//object defaults
console.log('\nDefault')
//defaultProps
const defaultProps = { company: 'MountBlue',City : 'Banglore',age:25};

const objdefault = object.defaults(objData,defaultProps)
console.log(objdefault)
